﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using ButterFlareCore.Models;

namespace ButterFlareCore.Controllers
{
    public class ChatController : Controller
    {
        AppDataBase database;

        public ChatController(AppDataBase context)
        {
            database = context;
        }

        [Authorize]
        public IActionResult Index()
        {
            string email = HttpContext.User.Claims.ToArray()[0].Value;
            User user = database.Users.Where(u => u.email == email).SingleOrDefault();
            ViewBag.User = user;

            return View();
        }
    }
}