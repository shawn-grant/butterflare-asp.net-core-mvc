﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ButterFlareCore.Models;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Collections;

namespace ButterFlareCore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        readonly AppDataBase database;

        public HomeController(AppDataBase context, ILogger<HomeController> logger)
        {
             database = context;
            _logger = logger;
        }

        [Authorize]
        public ActionResult Index()
        {
            string err = (string)TempData["Err"];
            ViewBag.Error = err;
            ViewBag.Posts = Enumerable.Reverse(database.Posts).ToArray();
            ViewBag.Users = database.Users.ToArray();
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult CreatePost(IFormFile file, string caption)
        {
            string email = HttpContext.User.Claims.ToArray()[0].Value;
            User user = database.Users.Where(u => u.email == email).SingleOrDefault();

            int lastID = (database.Posts.Any() == false) ? 0 : database.Posts.ToArray().Last().id;
            byte[] array = new byte[] { };

            if (file != null && file.Length > 0 && caption != null && caption.Length > 0)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    file.OpenReadStream().CopyTo(ms);
                    array = ms.GetBuffer();
                }

                database.Posts.Add(
                    new Post { id = lastID + 1, UID = user.UID, caption = caption, image = array });
                database.SaveChanges();
            }
            else
            {
                TempData["Err"] = "CAPTION OR IMAGE CANNOT BE LEFT BLANK.";
            }

            return RedirectToAction("Index");
        }


        public ActionResult About()
        {
            ViewBag.Message = "This is a simple social media web app";

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
