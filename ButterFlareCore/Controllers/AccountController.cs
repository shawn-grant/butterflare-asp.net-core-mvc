﻿using ButterFlareCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace ButterFlare.Views
{
    public class AccountController : Controller
    {
        public bool loggedIn = false;
        AppDataBase database;

        public AccountController(AppDataBase context)
        {
            database = context;
        }

        // GET: Account
        public ActionResult Login()
        {
            if (HttpContext.User != null && HttpContext.User.Identity.IsAuthenticated)
            {
                loggedIn = true;
            }

            if (loggedIn)
            {
                return RedirectToAction("Profile");
            }
            return View();
        }

        [HttpPost]
        public ActionResult LoginToAccount(string email, string password)
        {
            if (HttpContext.User != null && HttpContext.User.Identity.IsAuthenticated)
            {
                loggedIn = true;
            }

            if (!loggedIn)
            {
                PasswordHasher hasher = new PasswordHasher();

                //check database
                if (database.Users.Where(u => u.email == email).SingleOrDefault() != null)
                {
                    //user exists
                    User user = database.Users.Where(u => u.email == email).SingleOrDefault();

                    //check password against the one in the database
                    if (password != null && hasher.VerifyHashedPassword(user.password, password) == PasswordVerificationResult.Success)
                    {
                        //attempt login
                        var claims = new[]
                        {
                            new Claim("email", user.email),
                            new Claim("username", user.username),
                            new Claim("uid", user.UID.ToString())
                        };
                        var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                        HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity));
                        
                        return RedirectToAction("Profile");
                    }
                    else
                    {
                        //failed
                        Debug.WriteLine("Incorrect password");
                        ViewBag.Error = "INCORRECT PASSWORD";
                    }
                }
                else
                {
                    Debug.WriteLine("User Doesnt exist!");
                    ViewBag.Error = "NO SUCH USER";
                }
            }
            else { return RedirectToAction("Profile"); }

            return View("Login", ViewBag) ;
        }

        public ActionResult SignUp()
        {
            if (HttpContext.User != null && HttpContext.User.Identity.IsAuthenticated)
            {
                loggedIn = true;
            }
            if (loggedIn)
            {
                return RedirectToAction("Profile");
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateAccount(string uname, string email, string password, string confirmPassword)
        {
            PasswordHasher hasher = new PasswordHasher();

            //check database
            if (database.Users.Where(u => u.email == email).SingleOrDefault() != null ||
                database.Users.Where(u => u.username == uname).SingleOrDefault() != null)
            {
                //user already exists
                Debug.WriteLine("failed");
                ViewBag.Error = "User already exists";
            }
            else
            {
                if ((password != null && confirmPassword != null) && password == confirmPassword)
                {
                    Guid userID = Guid.NewGuid();

                    while (database.Users.Where(u => u.UID == userID).SingleOrDefault() != null)
                    {
                        userID = Guid.NewGuid();
                    }
                    string hashedPassword = hasher.HashPassword(password);
                    database.Users.Add(new User { UID =  userID, username = uname, email = email, password = hashedPassword});
                    database.SaveChanges();
                    return View("Login");
                }
                else
                {
                    Debug.WriteLine("failed");
                    ViewBag.Error = "passwords dont match";
                }
            }

            return View("SignUp", ViewBag);
        }
        
        [Authorize]
        public ActionResult Profile()
        {
            string email = HttpContext.User.Claims.ToArray()[0].Value;

            User user = database.Users.Where(u => u.email == email).SingleOrDefault();

            Post[] myPosts = database.Posts.Where(p => p.UID == user.UID).ToArray();
            ViewBag.Posts = myPosts;
            ViewBag.User = user;
            return View();
        }

        public ActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return View("Login");
        }
    }
}