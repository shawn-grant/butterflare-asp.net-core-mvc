using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Http;
using ButterFlareCore.Models;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using ButterFlareCore.Controllers;

namespace ButterFlareCore
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        IWebHostEnvironment env;

        public Startup(IConfiguration configuration, IWebHostEnvironment e)
        {
            Configuration = configuration;
            env = e;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
                //options.AutomaticChallenge = true;
                options.Cookie.Name = "ButterFlare.Auth.Cookie";
                options.LoginPath = "/login/";
            });
            services.AddHttpContextAccessor();
            services.AddEntityFrameworkSqlServer();
            services.AddDbContext<AppDataBase>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("ButterFlareDB").
                    Replace("%CONTENTROOTPATH%", env.ContentRootPath))
            );
            services.AddSignalR();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCors();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                //signal
                endpoints.MapHub<ChatHub>("signalchat");
                
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "about",
                    pattern: "about/",
                    defaults: new { controller = "Home", action = "About" });

                endpoints.MapControllerRoute(
                    name: "login",
                    pattern: "login/",
                    defaults: new { controller = "Account", action = "Login" });

                endpoints.MapControllerRoute(
                    name: "signup",
                    pattern: "signup/",
                    defaults: new { controller = "Account", action = "SignUp" });

                endpoints.MapControllerRoute(
                    name: "profile",
                    pattern: "profile/",
                    defaults: new { controller = "Account", action = "Profile" });
                endpoints.MapControllerRoute(
                    name: "chat",
                    pattern: "chat/",
                    defaults: new { controller = "Chat", action = "Index" });
            });
        }
    }
}
